#include "pch.h"

#include "Utils.h"

#include <fstream>
#include <vector>
#include <exception>
#include <iterator>

#include "SimpleDataProvider.h"

std::vector<char> utils::readFile(const std::string& path)
{
  std::ifstream stream(path, std::ios::in | std::ios::binary);
  if (!stream.is_open())
    throw std::runtime_error(std::string("Unable to open ") + path);

  // Stop eating new lines in binary mode!!!
  stream.unsetf(std::ios::skipws);

  std::vector<char> bytes;

  std::copy(std::istream_iterator<char>(stream),
    std::istream_iterator<char>(),
    std::back_inserter(bytes));

  return bytes;
}

core::BlockHashComputerPtr utils::createSequentialComputer(const std::vector<char>& data, size_t blockSizeMb)
{
  std::unique_ptr<SimpleDataProvider> pDataProvider(new SimpleDataProvider(data));

  auto result = std::make_unique<core::BlockHashComputer>(std::move(pDataProvider), blockSizeMb);
  return std::move(result);
}