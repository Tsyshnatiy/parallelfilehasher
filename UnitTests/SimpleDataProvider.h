#pragma once

#include <Core/IDataProvider.h>

namespace utils
{
  class SimpleDataProvider : public core::IDataProvider
  {
  public:
    SimpleDataProvider(const std::vector<char>& bytes);

    size_t dataLength() const override;
    bool isOpened() const override;
    std::vector<char> getNextBlock(size_t blockLength) override;
    bool isFinished() const override;

  private:
    const std::vector<char> m_data;
    size_t m_currentPosition = 0;
  };
}