#include "pch.h"

#include <fstream>
#include <iterator>
#include <algorithm>

#include <gtest/gtest.h>

#include <Core/BufferedReader.h>

#include "Utils.h"

using namespace core;
using namespace utils;

TEST(BufferedReaderTest, shouldCorrectlyReadHelloWorld) 
{
  std::string filePath("inputs/input1.txt");
  const auto entireFile = readFile(filePath);

  BufferedReader reader(filePath, { 0, entireFile.size() });
  EXPECT_EQ(reader.isOpened(), true);
  EXPECT_EQ(reader.isFinished(), false);

  const size_t block1Size = 3;
  const auto block1 = reader.getNextBlock(block1Size);
  EXPECT_EQ(block1.size(), block1Size);

  const auto isBlock1Ok = std::equal(block1.begin(), block1.end(), entireFile.begin());
  EXPECT_TRUE(isBlock1Ok);

  // Definitely more than file has
  // Chunk should be returnes
  const size_t block2Size = entireFile.size() + 100;
  const auto block2 = reader.getNextBlock(block2Size);
  EXPECT_EQ(block2.size(), entireFile.size() - block1Size);
  const auto isBlock2Ok = std::equal(block2.begin(), block2.end(), entireFile.begin() + block1Size);

  EXPECT_TRUE(isBlock2Ok);
  EXPECT_EQ(reader.isFinished(), true);
}