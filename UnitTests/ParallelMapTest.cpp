#include "pch.h"

#include <fstream>
#include <random>

#include <gtest/gtest.h>

#include <Core/FileParallelMap.h>
#include <Core/Utils.h>
#include <Core/Hasher.h>

#include "Utils.h"

using namespace core;
using namespace utils;

class ParallelHashingFixture : public ::testing::Test
{
public:
  std::string filename() const
  {
    return m_filename;
  }

  size_t dataSizeMb() const
  {
    return m_dataSizeMb;
  }

protected:
  void SetUp() override
  {
    const auto bytes = generateData(core::toBytes(m_dataSizeMb));
    saveDataIntoFile(m_filename, bytes);
  }

  void TearDown() override
  {
    std::remove(m_filename.c_str());
  }

private:
  template <typename OutputIt, typename Engine = std::mt19937>
  void generate(OutputIt first, OutputIt last)
  {
    static Engine engine;
    std::bernoulli_distribution distribution;

    while (first != last)
    {
      *first++ = distribution(engine);
    }
  }

  std::vector<char> generateData(size_t length)
  {
    std::vector<char> result;
    result.resize(length);
    generate(result.begin(), result.end());

    return result;
  }

  void saveDataIntoFile(const std::string& path, const std::vector<char>& data)
  {
    std::ofstream stream(path, std::ios::out | std::ios::binary);
    if (!stream.is_open())
      throw std::runtime_error(std::string("Unable to create ") + path);

    stream.write(data.data(), sizeof(char) * data.size());
    stream.flush();
    stream.close();
  }

  const std::string m_filename = "inputs/input2.bin";
  const size_t m_dataSizeMb = 50;
};


TEST_F(ParallelHashingFixture, shouldCorrectlyComputeHashes)
{
  const auto data = readFile(filename());

  const std::vector<size_t> blockSizes{ 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, dataSizeMb() + 10 };

  for (const auto blockSizeMb : blockSizes)
  {
    const auto seqHasher = createSequentialComputer(data, blockSizeMb);
    seqHasher->execute();
    const auto seqHashes = seqHasher->result();

    FileParallelMap parHashes(filename(), blockSizeMb);
    parHashes.run();

    const auto& result = parHashes.result();

    EXPECT_FALSE(result.hasError);
    EXPECT_EQ(result.blockHashes.size(), seqHashes.size());

    const auto areHashesEqual = std::equal(seqHashes.begin(), seqHashes.end(), result.blockHashes.begin());
    EXPECT_TRUE(areHashesEqual);
  }
}