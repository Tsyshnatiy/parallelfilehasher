#pragma once

#include <Core/BlockHashComputer.h>

namespace utils
{
  std::vector<char> readFile(const std::string& path);

  core::BlockHashComputerPtr createSequentialComputer(const std::vector<char>& data, size_t blockSizeMb);
}