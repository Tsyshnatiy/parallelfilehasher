#include "pch.h"

#include "SimpleDataProvider.h"

using namespace utils;

SimpleDataProvider::SimpleDataProvider(const std::vector<char>& bytes)
  : m_data(bytes)
{}

size_t SimpleDataProvider::dataLength() const
{
  return m_data.size();
}

bool SimpleDataProvider::isOpened() const
{
  return true;
}

std::vector<char> SimpleDataProvider::getNextBlock(size_t blockLength)
{
  auto end = m_currentPosition + blockLength;
  if (end > m_data.size())
    end = m_data.size();

  std::vector<char> result(m_data.begin() + m_currentPosition, m_data.begin() + end);
  m_currentPosition = end;

  return result;
}

bool SimpleDataProvider::isFinished() const
{
  return m_currentPosition == dataLength();
}