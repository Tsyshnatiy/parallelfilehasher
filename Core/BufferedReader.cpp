#include "BufferedReader.h"

#include <algorithm>

#include "Types.h"
#include "Utils.h"

using namespace core;

BufferedReader::BufferedReader(const std::string& filepath, const ByteInterval& interval)
  : m_currentFilePosition(interval.first),
  m_interval(interval),
  m_stream(std::ifstream(filepath, std::ios::in | std::ios::binary))
{
  if (isOpened())
  {
    // Stop eating new lines in binary mode!!!
    // https://stackoverflow.com/questions/15138353/how-to-read-a-binary-file-into-a-vector-of-unsigned-chars/21802936
    m_stream.unsetf(std::ios::skipws);
    m_stream.seekg(m_currentFilePosition);
  }
}

bool BufferedReader::isOpened() const
{
  return m_stream.is_open();
}

std::vector<char> BufferedReader::getNextBlock(size_t blockLength)
{
  if (!isOpened())
    return {};

  if (!hasData())
    readFromDisk();

  const auto bytesToEnd = m_cachedChunk.size() - m_cachedChunkPosition;
  const auto length = std::min(blockLength, bytesToEnd);
  if (length == 0)
    return {}; // may appear in finished state

  const auto beginIt = m_cachedChunk.cbegin() + m_cachedChunkPosition;
  std::vector<char> result(beginIt, beginIt + length);
  m_cachedChunkPosition += length;

  return result;
}

bool BufferedReader::isFinished() const
{
  return m_currentFilePosition == m_interval.second && !hasData();
}

bool BufferedReader::hasData() const
{
  return m_cachedChunkPosition < m_cachedChunk.size();
}

void BufferedReader::readFromDisk()
{
  // Read from disk and put data into the cache
  const auto intervalEnd = m_interval.second;
  const auto chunkSizeBytes = toBytes(c_cachedChunkSizeMb);
  const auto bytesToEnd = intervalEnd - m_currentFilePosition;

  const auto length = std::min(chunkSizeBytes, bytesToEnd);
  m_cachedChunk = std::vector<char>(length, 0);
  m_stream.read(m_cachedChunk.data(), m_cachedChunk.size());
  m_cachedChunkPosition = 0;

  m_currentFilePosition += length;
}

size_t BufferedReader::dataLength() const
{
  return m_interval.second - m_interval.first;
}