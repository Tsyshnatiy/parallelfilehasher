#pragma once

#include <vector>

namespace core
{
  class IJobExecutor;

  /// Executes added jobs in parallel
  class ParallelProcessor
  {
  public:
    /// Represents error if one of added jobs was not executed properly. Empty otherwise.
    struct ErrorState
    {
      bool errorState = false;
      std::string errorMessage;
    };

    void addJob(IJobExecutor* pJob);

    ErrorState process() const;

  private:
    std::vector<IJobExecutor*> m_jobs;
  };
}