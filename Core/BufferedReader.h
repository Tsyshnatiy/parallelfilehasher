#pragma once

#include <fstream>

#include "IDataProvider.h"
#include "Types.h"

namespace core
{
  /// Class for reading portions of data from a given file and byte interval
  class BufferedReader : public IDataProvider
  {
  public:
    BufferedReader(const std::string& filepath, const ByteInterval& interval);

    bool isOpened() const override;

    /**
    * Returns next chunk of data.
    * May read from disk if has not enough data in the internal cache.
    * Or may return chunk of the internal cache.
    */
    std::vector<char> getNextBlock(size_t blockLength) override;

    bool isFinished() const override;
    
    /// Returns length of given interval
    size_t dataLength() const override;

  private:
    bool hasData() const;

    void readFromDisk();

    const ByteInterval m_interval;
    size_t m_cachedChunkPosition = 0;
    size_t m_currentFilePosition = 0;
    std::vector<char> m_cachedChunk;
    static constexpr size_t c_cachedChunkSizeMb = 25; //mb

    std::ifstream m_stream;
  };
}