#pragma once

#include <memory>

#include "IJobExecutor.h"
#include "Types.h"

namespace core
{
  class IDataProvider;

  /// Class for computing hash from one block
  class BlockHashComputer : public IJobExecutor
  {
  public:
    /// Takes ownership of pDataProvider and saves blockSize
    BlockHashComputer(std::unique_ptr<IDataProvider>&& pDataProvider,
                      size_t blockSizeMb);

    BlockHashComputer(BlockHashComputer&& other);
    BlockHashComputer& operator = (BlockHashComputer&&);

    /// Starts block hash computation
    void execute() override;

    /// Returns computed hashes collection
    std::vector<Hash> result() const;

  private:
    std::unique_ptr<IDataProvider> m_pDataProvider;
    std::vector<Hash> m_result;
    const size_t m_blockSizeMb = 0;

    static constexpr size_t c_hasherBlockSizeMb = 1;
  };

  using BlockHashComputerPtr = std::unique_ptr<BlockHashComputer>;
}