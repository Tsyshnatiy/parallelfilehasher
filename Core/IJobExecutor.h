#pragma once

namespace core
{
  /// Job abstraction
  class IJobExecutor
  {
  public:
    virtual ~IJobExecutor() = default;

    virtual void execute() = 0;
  };
}