#include "BlockHashComputer.h"

#include "IDataProvider.h"
#include "Hasher.h"
#include "Utils.h"

using namespace core;

BlockHashComputer::BlockHashComputer(std::unique_ptr<IDataProvider>&& pDataProvider,
                                     size_t blockSizeMb)
  : m_pDataProvider(std::move(pDataProvider)),
  m_blockSizeMb(blockSizeMb)
{}

BlockHashComputer::BlockHashComputer(BlockHashComputer&& other)
  : m_pDataProvider(std::move(other.m_pDataProvider)),
  m_result(std::exchange(other.m_result, {}))
{
}

BlockHashComputer& BlockHashComputer::operator = (BlockHashComputer&& other)
{
  m_pDataProvider = std::move(other.m_pDataProvider);
  m_result = std::exchange(other.m_result, {});

  return *this;
}

std::vector<Hash> BlockHashComputer::result() const
{
  return m_result;
}

void BlockHashComputer::execute()
{
  if (!m_pDataProvider->isOpened())
    throw std::runtime_error("Data provider is not opened");

  const auto approximateHasherBlocks = m_pDataProvider->dataLength() / toBytes(c_hasherBlockSizeMb);
  std::vector<Hash> hashes;
  hashes.reserve(approximateHasherBlocks);

  // Get small chunks (c_hasherBlockSizeMb in size) until data provider has something and compute hashes on them
  // The reason of reading data using small chunks and not by blockSize is to save ram memory.
  // User could set block size equal to 1000 mb and we are done then.
  while (!m_pDataProvider->isFinished())
  {
    const auto hasherChunk = m_pDataProvider->getNextBlock(toBytes(c_hasherBlockSizeMb));
    if (hasherChunk.empty())
      break;

    const auto hash = Hasher::computeHash(hasherChunk);
    hashes.push_back(hash);
  }

  // Estimate number of blocks that need to be processed
  const auto approximateBlocks = m_pDataProvider->dataLength() / toBytes(m_blockSizeMb);
  m_result.reserve(approximateBlocks);

  // Assume here m_blockSizeMb % c_hasherBlockSizeMb == 0 for simplicity
  const auto hashesPerBlock = m_blockSizeMb / c_hasherBlockSizeMb;

  // Go over small chunk hashes and combine hashes from the same block to obtain one hash for one block
  for (size_t i = 0; i < hashes.size(); i += hashesPerBlock)
  {
    Hash seed = 0;
    for (size_t j = i; j < hashes.size() && j < i + hashesPerBlock; ++j)
      Hasher::hashCombine(seed, hashes[j]);

    m_result.push_back(seed);
  }
}