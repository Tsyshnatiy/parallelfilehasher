#pragma once

#include <vector>

namespace core
{
  /// Data source abstraction
  class IDataProvider
  {
  public:
    virtual ~IDataProvider() = default;
    
    virtual size_t dataLength() const = 0;
    virtual bool isOpened() const = 0;
    virtual std::vector<char> getNextBlock(size_t blockLength) = 0;
    virtual bool isFinished() const = 0;
  };
}