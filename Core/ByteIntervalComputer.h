#pragma once

#include "Types.h"

namespace core
{
  /// Class for splitting data into intervals
  class ByteIntervalComputer
  {
  public:
    ByteIntervalComputer(const Length dataLength,
                         const Length singleBlockLen);

    /// Takes m_dataLength and creates intervals of length m_singleBlockLen
    /// Last block may be less that m_singleBlockLen
    ByteIntervals computeIntervals() const;

  private:
    const Length m_dataLength = 0;
    const Length m_singleBlockLen = 0;
  };
}