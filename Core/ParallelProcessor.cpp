#include "ParallelProcessor.h"

#include "WorkerThread.h"

using namespace core;

void ParallelProcessor::addJob(IJobExecutor* pJob)
{
  m_jobs.push_back(pJob);
}

ParallelProcessor::ErrorState ParallelProcessor::process() const
{
  ErrorState result;

  std::vector<WorkerThreadPtr> workers;
  workers.reserve(m_jobs.size());

  for (const auto& job : m_jobs)
  {
    auto worker = std::make_unique<WorkerThread>(job);
    workers.push_back(std::move(worker));
    workers.back()->run();
  }

  // Enchancement: Cancellation token for cancelling jobs in case on an error
  for (const auto& pWorker : workers)
    pWorker->join();

  for (const auto& pWorker : workers)
  {
    if (pWorker->errorState())
    {
      result.errorState = true;
      result.errorMessage = pWorker->errorMessage();
      break;
    }
  }

  return result;
}