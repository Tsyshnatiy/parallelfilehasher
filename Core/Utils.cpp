#include "Utils.h"

std::ifstream::pos_type core::computeFileSize(const std::string& path)
{
  std::ifstream in(path, std::ifstream::ate | std::ifstream::binary);
  return in.tellg();
}