#pragma once

#include <fstream>
#include <string>

namespace core
{
  std::ifstream::pos_type computeFileSize(const std::string& path);

  inline size_t toBytes(size_t megabytes) { return megabytes * 1024 * 1024; }
}