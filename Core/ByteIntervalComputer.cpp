#include "ByteIntervalComputer.h"

#include <algorithm>

#include "Utils.h"

using namespace core;

ByteIntervalComputer::ByteIntervalComputer(const Length dataLength,
                                            const Length singleBlockLen)
  : m_dataLength(dataLength),
  m_singleBlockLen(singleBlockLen)
{

}

ByteIntervals ByteIntervalComputer::computeIntervals() const
{
  ByteIntervals result;

  Length begin = 0;
  Length end = begin;
  for (size_t i = 0; end < m_dataLength; ++i)
  {
    end = begin + m_singleBlockLen;
    if (end > m_dataLength)
      end = m_dataLength;

    result.emplace_back(begin, end);
    begin = end;
  }

  return result;
}