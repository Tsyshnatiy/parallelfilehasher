#include "WorkerThread.h"

#include <exception>

#include "IJobExecutor.h"

using namespace core;

WorkerThread::WorkerThread(IJobExecutor* pExecutor)
  : m_pExecutor(pExecutor),
  m_future(),
  m_errorState(false)
{}

WorkerThread::~WorkerThread()
{
  join();
}

WorkerThread::WorkerThread(WorkerThread&& other)
  : m_pExecutor(std::exchange(other.m_pExecutor, nullptr)),
  m_future(std::move(other.m_future)),
  m_errorState(std::exchange(other.m_errorState, false)),
  m_errorMessage(std::exchange(other.m_errorMessage, ""))
{}

WorkerThread& WorkerThread::operator = (WorkerThread&& other)
{
  m_pExecutor = std::exchange(other.m_pExecutor, nullptr);
  m_future = std::move(other.m_future);
  m_errorState = std::exchange(other.m_errorState, false);
  m_errorMessage = std::exchange(other.m_errorMessage, "");

  return *this;
}

void WorkerThread::run()
{
  m_future = std::async(std::launch::async, [this] { execute(); });
}

void WorkerThread::join()
{
  // Safe because it is always valid future,
  // Also it can not throw because of catch ...
  if (m_future.valid())
    m_future.get();
}

bool WorkerThread::errorState() const
{
  return m_errorState;
}

std::string WorkerThread::errorMessage() const
{
  return m_errorMessage;
}

void WorkerThread::execute()
{
  try
  {
    m_pExecutor->execute();
  }
  catch (const std::exception& e)
  {
    m_errorMessage = std::string(e.what());
    m_errorState = true;
  }
}