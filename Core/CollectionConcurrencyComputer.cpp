#include "CollectionConcurrencyComputer.h"

#include <thread>
#include <algorithm>

#include "Utils.h"

using namespace core;

CollectionConcurrencyComputer::CollectionConcurrencyComputer(size_t collectionSize, size_t blockSize)
  : m_collectionSize(collectionSize),
  m_blockSize(blockSize)
{
  init();
}

size_t CollectionConcurrencyComputer::blocksPerThread() const
{
  return m_blocksPerThread;
}

size_t CollectionConcurrencyComputer::numThreads() const
{
  return m_numThreads;
}

void CollectionConcurrencyComputer::init()
{
  const unsigned long long hardwareConc = std::thread::hardware_concurrency();

  const auto blocksNumber = (m_collectionSize % m_blockSize == 0)
    ? m_collectionSize / m_blockSize
    : m_collectionSize / m_blockSize + 1;

  const auto maxThreads = blocksNumber;
  m_numThreads = std::min(hardwareConc != 0 ? hardwareConc : 2, maxThreads);
  m_blocksPerThread = blocksNumber / m_numThreads;
}