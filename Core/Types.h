#pragma once

#include <vector>

namespace core
{
  using Hash = size_t;
  using Length = unsigned long long;
  using ByteInterval = std::pair<Length, Length>;
  using ByteIntervals = std::vector<ByteInterval>;
}