#include "FileParallelMap.h"

#include "Utils.h"
#include "ByteIntervalComputer.h"
#include "BlockHashComputer.h"
#include "BufferedReader.h"
#include "CollectionConcurrencyComputer.h"
#include "ParallelProcessor.h"

using namespace core;

FileParallelMap::FileParallelMap(const std::string& filepath, const size_t blockSizeMb)
  : m_filepath(filepath),
  m_blockSizeMb(blockSizeMb)
{}

void FileParallelMap::run()
{
  // Code below determines how many threads cpu supports,
  // determines file size and distributes non intersecting file intervals to threads.
  const auto fileLength = core::computeFileSize(m_filepath);
  const auto blockSizeBytes = toBytes(m_blockSizeMb);
  const CollectionConcurrencyComputer concurrencyComputer(fileLength, blockSizeBytes);
  const auto singleThreadChunkLength = concurrencyComputer.blocksPerThread() * blockSizeBytes;
  const core::ByteIntervalComputer byteIntervalComputer(fileLength, singleThreadChunkLength);
  const auto intervals = byteIntervalComputer.computeIntervals();
  
  std::vector<BlockHashComputer> executors;
  executors.reserve(intervals.size());

  // Add jobs into parallel processor
  ParallelProcessor parallelProc;
  for (const auto& interval : intervals)
  {
    auto pDataProvider = std::make_unique<BufferedReader>(m_filepath, interval);
    executors.emplace_back(std::move(pDataProvider), m_blockSizeMb);
    
    parallelProc.addJob(&executors.back());
  }

  // Start all added jobs and check for errors
  const auto errorState = parallelProc.process();
  if (errorState.errorState)
  {
    m_result.blockHashes.clear();
    m_result.hasError = true;
    m_result.error = errorState.errorMessage;
    return;
  }

  // Collect results from executors and add them into output result
  for (const auto& executor : executors)
  {
    const auto& executorResult = executor.result();
    auto& hashes = m_result.blockHashes;
    hashes.insert(hashes.end(), executorResult.cbegin(), executorResult.cend());
  }
}

FileParallelMap::ResultOrError FileParallelMap::result() const
{
  return m_result;
}
