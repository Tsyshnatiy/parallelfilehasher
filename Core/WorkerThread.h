#pragma once

#include <future>

#include "Types.h"

namespace core
{
  class IJobExecutor;

  /// Worker for executing jobs represented by IJobExecutor. Provides exception safety
  class WorkerThread
  {
  public:
    explicit WorkerThread(IJobExecutor* pExecutor);
    ~WorkerThread();

    WorkerThread(const WorkerThread&) = delete;
    WorkerThread& operator = (const WorkerThread&) = delete;

    WorkerThread(WorkerThread&&);
    WorkerThread& operator = (WorkerThread&&);

    /// Runs job provided by the constructor
    void run();

    /// Waits if needed until job is done and returns
    void join();

    /// Signals if job was not successfully done
    bool errorState() const;

    /// Returns detailed description of what gone wrong with the job
    std::string errorMessage() const;

  private:
    void execute();

    IJobExecutor* m_pExecutor = nullptr;

    std::future<void> m_future;
    bool m_errorState = false;
    std::string m_errorMessage;
  };

  using WorkerThreadPtr = std::unique_ptr<WorkerThread>;
}