#include "Hasher.h"

using namespace core;

// Piece of implementation from boost
void Hasher::hashCombine(Hash& seed, const Hash& hash)
{
  seed ^= hash + 0x9e3779b9 + (seed << 6) + (seed >> 2);
}

Hash Hasher::computeHash(const std::vector<char>& bytes)
{
  // http://www.cse.yorku.ca/~oz/hash.html
  size_t h = 5381;
  for (const auto byte : bytes)
    h = ((h << 5) + h) + byte;

  return h;
}