#pragma once

#include "Types.h"

namespace core
{
  /// Computes and combines hashes
  class Hasher
  {
  public:
    static Hash computeHash(const std::vector<char>& bytes);
    static void hashCombine(Hash& seed, const Hash& hash);
  };
}