#pragma once

namespace core
{
  /// Helper class for computing how many blocks can be processed by each thread
  class CollectionConcurrencyComputer
  {
  public:
    CollectionConcurrencyComputer(size_t collectionSize, size_t blockSize);

    size_t blocksPerThread() const;
    size_t numThreads() const;

  private:
    void init();

    const size_t m_collectionSize = 0;
    const size_t m_blockSize = 0;

    size_t m_blocksPerThread = 0;
    size_t m_numThreads = 0;
  };
}