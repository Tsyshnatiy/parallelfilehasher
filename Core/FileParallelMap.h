#pragma once

#include <string>
#include <vector>
#include <memory>

#include "Types.h"
#include "WorkerThread.h"

namespace core
{
  /// Class for mapping each block into separate hash
  class FileParallelMap
  {
  public:
    /// Struct that contains result (one hash per one block) or error if something went wrong.
    struct ResultOrError
    {
      // Result is stored in one collection because even 1 tb file will generate
      // 1024(gb in tb) * 1024(mb in one gb) * sizeof(size_t) (8 byte on 64 bit system) = 
      // = 8192 (kb) = 8 (mb) in the ram. So it is fine to store hashes in one collection.
      std::vector<Hash> blockHashes;
      std::string error;
      bool hasError = false;
    };

    FileParallelMap(const std::string& filepath, const size_t blockSizeMb);

    /// Starts hash computation.
    void run();

    /// Method for obtaining hash computation result.
    ResultOrError result() const;

  private:
    ResultOrError m_result;

    const std::string m_filepath;
    const size_t m_blockSizeMb;
  };
}