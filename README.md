# Parallel file hasher
This software computes hash of files that may not fit into memory.
This can be useful in case you have two big files and want to check
do they have equal content.
File processing is done in parallel.

## Arguments input format:
ParallelFileHasher.exe input=inp.txt blockSize=10 output=out.txt

[input]<br>
Input file name.<br>

[blockSize]<br>
One hash will be computed for one block.<br>
Blocksize is measured in megabytes.<br>
If argument is not provided then blockSize is assumed to be 1 mb.<br>
            
[output]<br>
Output file name. Output content is input file hash.<br>

## Solution structure
Solutions contains of 3 projects:
1. Core - static library where all logic is implemented
2. ParallelFileHasher - binary that uses Core to do the job
3. UnitTests - they do exactly what you expect

## Build
Build can be done via Visual Studio 2017.
Just open the solution and press build button.

## Documentation
doxygen doxy.conf