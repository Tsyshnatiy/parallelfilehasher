#include "CmdArgsParser.h"

#include <sstream>

const std::string CmdArgsParser::c_inputArgName = "input";
const std::string CmdArgsParser::c_blockSizeArgName = "blockSize";
const std::string CmdArgsParser::c_outputArgName = "output";

bool CmdArgsParser::tryParseInputFilename(const std::string& str, std::string& result)
{
  const auto findPos = str.find(c_inputArgName);
  if (findPos != 0)
    return false;

  result = str.substr(c_inputArgName.size() + 1); // +1 because of '='

  return true;
}

bool CmdArgsParser::tryParseOutputFilename(const std::string& str, std::string& result)
{
  const auto findPos = str.find(c_outputArgName);
  if (findPos != 0)
    return false;

  result = str.substr(c_outputArgName.size() + 1); // +1 because of '='

  return true;
}

bool CmdArgsParser::tryParseBlockSize(const std::string& str, size_t& result)
{
  const auto findPos = str.find(c_blockSizeArgName);
  if (findPos != 0)
    return false;

  size_t value = 0;
  const auto blockSizeStr = str.substr(c_blockSizeArgName.size() + 1); // +1 because of '='

  std::istringstream iss(blockSizeStr);
  iss >> value;
  if (iss.fail())
    return false;

  result = value;

  return true;
}

CmdArgsParser::Result CmdArgsParser::parse(int ac, char* av[])
{
  Result result;

  for (int i = 1; i < ac; ++i)
  {
    std::string ithAv(av[i]);

    if (tryParseInputFilename(ithAv, result.inputFileName))
      continue;
    else if (tryParseOutputFilename(ithAv, result.outputFileName))
      continue;
    else if (tryParseBlockSize(ithAv, result.blockSizeMb))
      continue;
  }

  return result;
}