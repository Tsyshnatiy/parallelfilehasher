#pragma once

#include <string>

/// Simple class for parsing command line arguments
class CmdArgsParser
{
public:
  /// Represents parsed commands line arguments
  struct Result
  {
    std::string inputFileName;
    std::string outputFileName;
    size_t blockSizeMb = 1;
  };

  static Result parse(int ac, char* av[]);

private:
  static bool tryParseInputFilename(const std::string& str, std::string& result);
  static bool tryParseOutputFilename(const std::string& str, std::string& result);
  static bool tryParseBlockSize(const std::string& str, size_t& blockSize);

  static const std::string c_inputArgName;
  static const std::string c_blockSizeArgName;
  static const std::string c_outputArgName;
};