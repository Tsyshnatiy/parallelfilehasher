#include <iostream>
#include <fstream>
#include <chrono>

#include <Core/FileParallelMap.h>
#include <Core/ByteIntervalComputer.h>
#include <Core/Utils.h>

#include "CmdArgsParser.h"

/** \mainpage Parallel file hasher description
* This software computes hash of files that may not fit into memory.<br>
* This can be useful in case you have two big files and want to check do they have equal content.<br>
* File processing is done in parallel.<br>
*
* \section intro_sec Arguments format
* ParallelFileHasher.exe input=inp.txt blockSize=10 output=out.txt<br>
*
* input<br>
* Argument where user should provide input file name.<br>
*
* blockSize<br>
* One hash will be computed for one block.<br>
* Blocksize is measured in megabytes.<br>
* If argument is not provided then blockSize is assumed to be 1 mb.<br>
*
* output<br>
* Argument where user should provide output file name.<br>
* Output content is input file hash.<br>
*
* \section run_examples Run examples
* Run example 1<br>
* ParallelFileHasher.exe input=inp.bin blockSize=10 output=out.bin<br>
*
* Run example 2<br>
* ParallelFileHasher.exe input=inp.bin output=out.bin<br>
*
* \section build_sec Build
* Solutions consists of 3 projects:<br>
* 1. Core - static library where all logic is implemented<br>
* 2. ParallelFileHasher - executable binary that uses Core to do the job<br>
* 3. UnitTests - they do exactly what you expect<br>
*
* Build can be done via Visual Studio 2017.<br>
* Just open the solution and press build button.<br>
*/

int main(int ac, char* av[])
{
  auto start = std::chrono::system_clock::now();
  
  const auto args = CmdArgsParser::parse(ac, av);

  std::cout
    << "Input: " << args.inputFileName
    << std::endl
    << "Blocksize: " << args.blockSizeMb << "mb"
    << std::endl
    << "Output: " << args.outputFileName
    << std::endl;

  core::FileParallelMap parMap(args.inputFileName, args.blockSizeMb);
  parMap.run();

  const auto result = parMap.result();
  if (result.hasError)
  {
    std::cout << result.error << std::endl;
    return 1;
  }

  std::ofstream out(args.outputFileName, std::ofstream::out | std::ios::binary);
  if (!out.is_open())
  {
    std::cout << "Unable to open output file" << std::endl;
    return 2;
  }

  const auto& hashes = result.blockHashes;
  if (!hashes.empty())
    out.write((char*)hashes.data(), sizeof(core::Hash) * hashes.size());

  out.flush();
  out.close();

  auto end = std::chrono::system_clock::now();
  auto duration = std::chrono::duration_cast<std::chrono::duration<float>>(end - start);
  std::cout << "Successfully finished in " << duration.count() << " seconds" << std::endl;

  return 0;
}